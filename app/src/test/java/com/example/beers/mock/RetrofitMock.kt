package com.example.beers.mock

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object RetrofitMock {

    inline fun <reified T : Any> create(url: String): T = getRetrofit(url).create(T::class.java)

    fun getRetrofit(url: String): Retrofit = getRetrofitBuilder(url).client(getOkHttpBuilder().build()).build()

    private fun getRetrofitBuilder(url: String): Retrofit.Builder = Retrofit.Builder()
        .baseUrl(url)
        .addConverterFactory(GsonConverterFactory.create())

    private fun getOkHttpBuilder(): OkHttpClient.Builder = OkHttpClient.Builder()
        .readTimeout(1, TimeUnit.MINUTES)
        .connectTimeout(1, TimeUnit.MINUTES)
        .connectTimeout(1, TimeUnit.MINUTES)
        .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
}
