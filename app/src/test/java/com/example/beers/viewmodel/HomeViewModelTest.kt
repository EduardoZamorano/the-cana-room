package com.example.beers.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.paging.PagingData
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.usecases.GetBeersUseCase
import com.example.beers.extensions.mock
import com.example.beers.presentation.viewmodels.home.HomeViewModel
import com.example.beers.rules.CoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class HomeViewModelTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Rule @JvmField val testRule: TestRule = InstantTaskExecutorRule()
    @ExperimentalCoroutinesApi @get:Rule var coroutineRule = CoroutineRule()
    @Mock private lateinit var getBeersUseCase: GetBeersUseCase
    private lateinit var homeViewModel: HomeViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        homeViewModel = HomeViewModel(getBeersUseCase)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersUseCaseWhenGetBeersCalled() = runTest {
        val data = mock<PagingData<BeersModel>>()
        val flow = flow { emit(data) }

        given(getBeersUseCase()).willReturn(flow)
        homeViewModel.getBeers()
        verify(getBeersUseCase).invoke()
    }
}
