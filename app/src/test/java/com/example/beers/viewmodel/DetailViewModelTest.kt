package com.example.beers.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.beers.utils.ResponseState
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.usecases.GetBeersByIdUseCase
import com.example.beers.extensions.assertIsInstanceOf
import com.example.beers.extensions.mock
import com.example.beers.presentation.viewmodels.detail.DetailViewModel
import com.example.beers.rules.CoroutineRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.net.UnknownHostException

class DetailViewModelTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Rule @JvmField val testRule: TestRule = InstantTaskExecutorRule()
    @ExperimentalCoroutinesApi @get:Rule var coroutineRule = CoroutineRule()
    @Mock private lateinit var getBeersByIdUseCase: GetBeersByIdUseCase
    private lateinit var detailViewModel: DetailViewModel

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        detailViewModel = DetailViewModel(getBeersByIdUseCase)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersByIdUseCaseWhenGetBeersByIdCalled() = runTest {
        val mockBeersModel = mock<List<BeersModel>>()
        val data = ResponseState.Success(mockBeersModel)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(getBeersByIdUseCase(id)).willReturn(flow)
        detailViewModel.getBeersById(id)
        verify(getBeersByIdUseCase).invoke(id)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersByIdUseCaseWhenGetBeersByIdCalledSuccess() = runTest {
        val mockBeersModel = mock<List<BeersModel>>()
        val data = ResponseState.Success(mockBeersModel)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(getBeersByIdUseCase(id)).willReturn(flow)
        detailViewModel.getBeersById(id)

        val stateDataSuccess = detailViewModel.beerState.value
        assertIsInstanceOf<ResponseState.Success<List<BeersModel>>>(stateDataSuccess)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersUseCaseWhenGetBeersCalledStateChangeSuccess() = runTest {
        val mockBeersModel = mock<List<BeersModel>>()
        val data = ResponseState.Success(mockBeersModel)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(getBeersByIdUseCase(id)).willReturn(flow)
        detailViewModel.getBeersById(id)

        val beerState = detailViewModel.beerState.value as ResponseState.Success
        assertEquals(ResponseState.Success(mockBeersModel), beerState)
        assertTrue((beerState).data is List<BeersModel>)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersUseCaseWhenGetBeersCalledError() = runTest {
        val exception = mock<UnknownHostException>()
        val data = ResponseState.Failure(exception)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(getBeersByIdUseCase(id)).willReturn(flow)
        detailViewModel.getBeersById(id)

        val stateDataError = detailViewModel.beerState.value as ResponseState.Failure
        assertIsInstanceOf<ResponseState.Failure>(stateDataError)
    }
}
