package com.example.beers.domain.usecase

import com.example.beers.domain.repository.BeersRepository
import com.example.beers.domain.usecases.DeleteRecentlyUseCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class DeleteRecentlyUseCaseTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Mock private lateinit var beersRepository: BeersRepository
    private lateinit var deleteRecentlyUseCase: DeleteRecentlyUseCase

    @Before
    fun setup() {
        deleteRecentlyUseCase = DeleteRecentlyUseCase(beersRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callDeleteAllWhenDeleteAllBeersExecuted() = runTest {
        deleteRecentlyUseCase()
        verify(beersRepository).deleteAll()
    }
}
