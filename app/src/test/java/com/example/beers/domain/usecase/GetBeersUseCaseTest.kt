package com.example.beers.domain.usecase

import androidx.paging.PagingData
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.repository.BeersRepository
import com.example.beers.domain.usecases.GetBeersUseCase
import com.example.beers.extensions.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class GetBeersUseCaseTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Mock private lateinit var beersRepository: BeersRepository
    private lateinit var getBeersUseCase: GetBeersUseCase

    @Before
    fun setup() {
        getBeersUseCase = GetBeersUseCase(beersRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersWhenGetBeersExecuted() = runTest {
        val data = mock<PagingData<BeersModel>>()
        val flow = flow { emit(data) }

        given(beersRepository.fetchBeers()).willReturn(flow)
        getBeersUseCase()
        verify(beersRepository).fetchBeers()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun receiveFetchBeersWhenGetBeersSuccessExecuted() = runTest {
        val data = mock<PagingData<BeersModel>>()
        val flow = flow { emit(data) }

        given(beersRepository.fetchBeers()).willReturn(flow)
        assertEquals(data, getBeersUseCase().first())
    }
}
