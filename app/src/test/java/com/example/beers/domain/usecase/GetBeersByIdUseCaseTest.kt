package com.example.beers.domain.usecase

import com.example.beers.utils.ResponseState
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.repository.BeersRepository
import com.example.beers.domain.usecases.GetBeersByIdUseCase
import com.example.beers.extensions.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule
import java.net.UnknownHostException

class GetBeersByIdUseCaseTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Mock private lateinit var beersRepository: BeersRepository
    private lateinit var getBeersByIdUseCase: GetBeersByIdUseCase

    @Before
    fun setup() {
        getBeersByIdUseCase = GetBeersByIdUseCase(beersRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersByIdWhenGetBeersByIdExecuted() = runTest {
        val mockBeersModel = mock<List<BeersModel>>()
        val data = ResponseState.Success(mockBeersModel)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(beersRepository.fetchBeersById(id)).willReturn(flow)
        getBeersByIdUseCase(id)
        verify(beersRepository).fetchBeersById(id)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetBeersByIdWhenGetBeersByIdNotExecuted() = runTest {
        val exception = mock<UnknownHostException>()
        val data = ResponseState.Failure(exception)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(beersRepository.fetchBeersById(id)).willReturn(flow)
        getBeersByIdUseCase(id)
        verify(beersRepository).fetchBeersById(id)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun receiveBeersWhenGetBeersByIdSuccessExecuted() = runTest {
        val mockBeersModel = mock<List<BeersModel>>()
        val data = ResponseState.Success(mockBeersModel)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(beersRepository.fetchBeersById(id)).willReturn(flow)
        assertEquals(data, getBeersByIdUseCase(id).first())
    }

    @ExperimentalCoroutinesApi
    @Test
    fun receiveBeersWhenGetBeersByIdErrorExecuted() = runTest {
        val exception = mock<UnknownHostException>()
        val data = ResponseState.Failure(exception)
        val flow = flow { emit(data) }
        val id = "Some value"

        given(beersRepository.fetchBeersById(id)).willReturn(flow)
        assertEquals(data, getBeersByIdUseCase(id).first())
    }
}
