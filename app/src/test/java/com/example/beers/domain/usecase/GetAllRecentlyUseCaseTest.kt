package com.example.beers.domain.usecase

import com.example.beers.domain.models.RecentlyBeersModel
import com.example.beers.domain.repository.BeersRepository
import com.example.beers.domain.usecases.GetAllRecentlyUseCase
import com.example.beers.extensions.mock
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.test.runTest
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.BDDMockito.given
import org.mockito.Mock
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class GetAllRecentlyUseCaseTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Mock private lateinit var beersRepository: BeersRepository
    private lateinit var getAllRecentlyUseCase: GetAllRecentlyUseCase

    @Before
    fun setup() {
        getAllRecentlyUseCase = GetAllRecentlyUseCase(beersRepository)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun callGetAllRecentlyWhenGetAllRecentlyExecuted() = runTest {
        val data = mock<List<RecentlyBeersModel>>()
        val flow = flow { emit(data) }

        given(beersRepository.getAllRecently()).willReturn(flow)
        getAllRecentlyUseCase()
        verify(beersRepository).getAllRecently()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun receiveRecentlyBeersWhenGetAllRecentlySuccessExecuted() = runTest {
        val data = mock<List<RecentlyBeersModel>>()
        val flow = flow { emit(data) }

        given(beersRepository.getAllRecently()).willReturn(flow)
        assertEquals(data, getAllRecentlyUseCase().first())
    }
}
