package com.example.beers.rules

import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import org.hamcrest.CoreMatchers.equalTo
import org.hamcrest.MatcherAssert.assertThat
import org.junit.rules.TestRule
import org.junit.runner.Description
import org.junit.runners.model.Statement

open class MockWebServerRule : TestRule {

    companion object {
        private const val GET = "GET"
    }

    val server = MockWebServer()

    override fun apply(base: Statement, description: Description): Statement {
        return object : Statement() {
            override fun evaluate() {
                server.start()
                base.evaluate()
                server.shutdown()
            }
        }
    }

    fun getBaseUrl(path: String = "/") = server.url(path).toString()

    fun givenMockResponse(responseCode: Int = 200) {
        val mockResponse = MockResponse()
        mockResponse.setResponseCode(responseCode)
        server.enqueue(mockResponse)
    }

    fun shutdown() {
        server.shutdown()
    }

    fun assertGetRequestSentTo(url: String) = getTakeRequest().run {
        assertThat(path, equalTo(url))
        assertThat(method, equalTo(GET))
    }

    private fun getTakeRequest() = server.takeRequest()
}
