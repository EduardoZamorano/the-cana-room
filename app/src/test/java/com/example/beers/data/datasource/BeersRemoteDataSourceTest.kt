package com.example.beers.data.datasource

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.beers.data.datasource.remote.BeersRemoteDataSource
import com.example.beers.data.datasource.remote.api.BeersApi
import com.example.beers.mock.RetrofitMock
import com.example.beers.rules.CoroutineRule
import com.example.beers.rules.MockWebServerRule
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule

class BeersRemoteDataSourceTest {

    @Rule @JvmField val mockitoRule: MockitoRule = MockitoJUnit.rule()
    @Rule @JvmField val mockWebServerRule = MockWebServerRule()
    @get: Rule var instantExecutorRule = InstantTaskExecutorRule()
    @ExperimentalCoroutinesApi @get:Rule var coroutineRule = CoroutineRule()
    private lateinit var remoteDataSource: BeersRemoteDataSource

    @Before
    fun setUp() {
        val beersService = RetrofitMock.create<BeersApi>(mockWebServerRule.getBaseUrl())
        remoteDataSource = BeersRemoteDataSource(beersService)
    }

    @After
    fun tearDown() {
        mockWebServerRule.shutdown()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun sendFetchBeerRequestToTheCorrectEndpoint() = runTest(coroutineRule.testDispatcher) {
        mockWebServerRule.givenMockResponse()

        val job = launch { remoteDataSource.fetchBeers(null) }

        mockWebServerRule.assertGetRequestSentTo("/v2/beers?per_page=20")
        job.cancel()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun sendFetchBeerPaginateRequestToTheCorrectEndpoint() = runTest(coroutineRule.testDispatcher) {
        mockWebServerRule.givenMockResponse()

        val job = launch { remoteDataSource.fetchBeers(page = 2) }

        mockWebServerRule.assertGetRequestSentTo("/v2/beers?page=2&per_page=20")
        job.cancel()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun sendFetchBeerByIdRequestToTheCorrectEndpoint() = runTest(coroutineRule.testDispatcher) {
        mockWebServerRule.givenMockResponse()

        val job = launch { remoteDataSource.fetchBeersById("1") }

        mockWebServerRule.assertGetRequestSentTo("/v2/beers/1")
        job.cancel()
    }
}
