package com.example.beers.database

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.platform.app.InstrumentationRegistry
import com.example.beers.data.datasource.local.AppDatabase
import com.example.beers.data.datasource.local.dao.RecentlyDao
import com.example.beers.data.datasource.local.entities.RecentlyEntity
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test

class RecentlyDaoTest {

    private lateinit var context: Context
    private lateinit var recentlyDao: RecentlyDao
    private lateinit var database: AppDatabase
    private val recently = RecentlyEntity(
        1, "https://images.punkapi.com/v2/keg.png", "Buzz", "A Real Bitter Experience.",
        "09/2007", "20 liters", 4.5, 60.0, 20.0, 10.0, 4.4, "Sam Mason <samjbmason>",
        "A light, crisp and bitter IPA brewed with English...", "Spicy chicken tikka masala",
        "The earthy and floral aromas from the hops"
    )
    private val recentlyTwo = RecentlyEntity(
        2, "https://images.punkapi.com/v2/keg.png", "Buzz", "A Real Bitter Experience.",
        "09/2007", "20 liters", 4.5, 60.0, 20.0, 10.0, 4.4, "Sam Mason <samjbmason>",
        "A light, crisp and bitter IPA brewed with English...", "Spicy chicken tikka masala",
        "The earthy and floral aromas from the hops"
    )
    @get:Rule val instantTaskExecutorRule = InstantTaskExecutorRule()

    /**
     * Initialize variables necessary to run Dao tests.
     *
     * [database] is initialize as [Room.inMemoryDatabaseBuilder]:
     * according to documentation this method creates a RoomDatabase.Builder for an in memory database.
     * Information stored in an in memory database disappears when the process is killed.
     * Once a database is built, you should keep a reference to it and re-use it.
     */
    @Before
    fun setUp() {
        context = InstrumentationRegistry.getInstrumentation().context
        database = Room.inMemoryDatabaseBuilder(context, AppDatabase::class.java).build()
        recentlyDao = database.recentlyDao()
    }

    @Test
    fun insertTest() = runBlocking {
        recentlyDao.insert(recently)

        val recentlyDb = recentlyDao.getAll().first()
        assertEquals(recently, recentlyDb)
    }

    @Test
    fun getAllTest() = runBlocking {
        recentlyDao.insert(recently)
        recentlyDao.insert(recentlyTwo)

        val recentlyDb = recentlyDao.getAll()
        assertEquals(2, recentlyDb.size)
    }

    @Test
    fun getAllEmptyTest() = runBlocking {
        val recentlyDb = recentlyDao.getAll()
        assertTrue(recentlyDb.isEmpty())
    }

    @Test
    fun deleteAllTest() = runBlocking {
        recentlyDao.insert(recently)
        recentlyDao.insert(recentlyTwo)

        assertEquals(2, recentlyDao.getAll().size)

        recentlyDao.deleteAll()
        assertTrue(recentlyDao.getAll().isEmpty())
    }

    @Test
    fun deleteAllEmptyTest() = runBlocking {
        recentlyDao.deleteAll()
        val dbSymbols = recentlyDao.getAll()
        assertTrue(dbSymbols.isEmpty())
    }

    /**
     * Close the DB when tests is finish.
     */
    @After
    fun tearDown() {
        database.close()
    }
}
