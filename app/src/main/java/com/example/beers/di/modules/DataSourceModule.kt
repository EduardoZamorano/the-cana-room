package com.example.beers.di.modules

import com.example.beers.data.datasource.local.dao.BeersDao
import com.example.beers.data.datasource.local.dao.RecentlyDao
import com.example.beers.data.datasource.local.dao.RemoteKeysDao
import com.example.beers.data.datasource.local.datasource.BeersLocalDataSource
import com.example.beers.data.datasource.remote.BeersRemoteDataSource
import com.example.beers.data.datasource.remote.api.BeersApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataSourceModule {

    @Singleton
    @Provides
    fun provideBeersRemoteDataSource(beersApi: BeersApi) = BeersRemoteDataSource(beersApi)

    @Singleton
    @Provides
    fun provideBeersLocalDataSource(recentlyDao: RecentlyDao, beersDao: BeersDao, remoteKeysDao: RemoteKeysDao) =
        BeersLocalDataSource(recentlyDao, beersDao, remoteKeysDao)
}
