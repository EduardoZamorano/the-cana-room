package com.example.beers.di.modules

import com.example.beers.data.repository.BeersRepositoryImpl
import com.example.beers.domain.repository.BeersRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class RepositoryModule {

    @Singleton
    @Binds
    abstract fun provideBeersRepository(repository: BeersRepositoryImpl): BeersRepository
}
