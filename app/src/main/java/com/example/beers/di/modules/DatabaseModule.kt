package com.example.beers.di.modules

import android.content.Context
import com.example.beers.data.datasource.local.AppDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Context) = AppDatabase.build(context)

    @Singleton
    @Provides
    fun provideRecentlyDao(appDatabase: AppDatabase) = appDatabase.recentlyDao()

    @Singleton
    @Provides
    fun provideBeersDao(appDatabase: AppDatabase) = appDatabase.beersDao()

    @Singleton
    @Provides
    fun provideRemoteKeysDao(appDatabase: AppDatabase) = appDatabase.getRemoteKeysDao()
}
