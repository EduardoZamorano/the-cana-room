package com.example.beers.di.modules

import com.example.beers.data.repository.BeersRepositoryImpl
import com.example.beers.domain.usecases.DeleteRecentlyUseCase
import com.example.beers.domain.usecases.GetAllRecentlyUseCase
import com.example.beers.domain.usecases.GetBeersByIdUseCase
import com.example.beers.domain.usecases.GetBeersUseCase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

@Module
@InstallIn(SingletonComponent::class)
class UsesCasesModule {

    @Provides
    fun provideGetBeersUseCase(beersRepository: BeersRepositoryImpl) = GetBeersUseCase(beersRepository)

    @Provides
    fun provideGetBeersByIdUseCase(beersRepository: BeersRepositoryImpl) = GetBeersByIdUseCase(beersRepository)

    @Provides
    fun provideGetAllRecentlyUseCase(beersRepository: BeersRepositoryImpl) = GetAllRecentlyUseCase(beersRepository)

    @Provides
    fun provideDeleteRecentlyUseCase(beersRepository: BeersRepositoryImpl) = DeleteRecentlyUseCase(beersRepository)
}
