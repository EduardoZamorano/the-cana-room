package com.example.beers.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class BeersModel(
    val abv: Double,
    val brewersTips: String,
    val contributedBy: String,
    val description: String,
    val ebc: Double,
    val firstBrewed: String,
    val foodPairing: String,
    val ibu: Double,
    val id: Int,
    val imageUrl: String?,
    val name: String,
    val ph: Double,
    val srm: Double,
    val tagline: String,
    val volume: String
) : Parcelable
