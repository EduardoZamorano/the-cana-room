package com.example.beers.domain.usecases

import com.example.beers.domain.repository.BeersRepository
import javax.inject.Inject

class GetAllRecentlyUseCase @Inject constructor(private val beersRepository: BeersRepository) {

    suspend operator fun invoke() = beersRepository.getAllRecently()
}
