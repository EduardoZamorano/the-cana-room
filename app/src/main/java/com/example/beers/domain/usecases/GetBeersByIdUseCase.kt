package com.example.beers.domain.usecases

import com.example.beers.domain.repository.BeersRepository
import javax.inject.Inject

class GetBeersByIdUseCase @Inject constructor(private val beersRepository: BeersRepository) {

    suspend operator fun invoke(id: String) = beersRepository.fetchBeersById(id)
}
