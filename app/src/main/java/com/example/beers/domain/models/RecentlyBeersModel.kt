package com.example.beers.domain.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class RecentlyBeersModel(
    val id: Int,
    val imageUrl: String?,
    val name: String,
    val tagline: String,
    val firstBrewed: String,
    val volume: String,
    val abv: Double,
    val ibu: Double,
    val ebc: Double,
    val srm: Double,
    val ph: Double,
    val contributedBy: String,
    val description: String,
    val foodPairing: String,
    val brewersTips: String
) : Parcelable
