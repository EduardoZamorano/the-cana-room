package com.example.beers.domain.usecases

import com.example.beers.domain.repository.BeersRepository
import javax.inject.Inject

class GetBeersUseCase @Inject constructor(private val beersRepository: BeersRepository) {

    operator fun invoke() = beersRepository.fetchBeers()
}
