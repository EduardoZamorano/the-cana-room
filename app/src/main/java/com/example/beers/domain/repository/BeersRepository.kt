package com.example.beers.domain.repository

import androidx.paging.PagingData
import com.example.beers.utils.ResponseState
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.models.RecentlyBeersModel
import kotlinx.coroutines.flow.Flow

interface BeersRepository {

    fun fetchBeers(): Flow<PagingData<BeersModel>>

    suspend fun fetchBeersById(id: String): Flow<ResponseState<List<BeersModel>>>

    suspend fun getAllRecently() : Flow<List<RecentlyBeersModel>>

    suspend fun deleteAll()
}
