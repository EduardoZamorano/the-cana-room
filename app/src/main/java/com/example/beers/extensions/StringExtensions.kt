package com.example.beers.extensions

fun String.Companion.space() = " "

fun String.Companion.empty() = ""
