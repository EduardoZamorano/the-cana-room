package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class MaltResponse(
    @SerializedName("amount") val amount: AmountResponse,
    @SerializedName("name")  val name: String
)
