package com.example.beers.data

import com.example.beers.data.datasource.local.entities.BeerEntity
import com.example.beers.data.datasource.local.entities.RecentlyEntity
import com.example.beers.data.datasource.remote.models.BeersModelItemResponse
import com.example.beers.data.datasource.remote.models.BeersResponse
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.models.RecentlyBeersModel
import com.example.beers.extensions.space

fun BeersResponse?.toBeers() = this?.map { it.toBeersModelItem() }

fun BeersModelItemResponse.toBeersModelItem() = BeersModel(
    abv = abv,
    brewersTips = brewersTips,
    contributedBy = contributedBy,
    description = description,
    ebc = ebc,
    firstBrewed = firstBrewed,
    foodPairing = foodPairing.joinToString(),
    ibu = ibu,
    id = id,
    imageUrl = imageUrl,
    name = name,
    ph = ph,
    srm = srm,
    tagline = tagline,
    volume = volume.value.toString() + String.space() + volume.unit
)

fun BeersResponse.toBeersEntity() = this.map { it.toBeersEntity() }

fun BeersModelItemResponse.toBeersEntity() = BeerEntity(
    id = id,
    imageUrl = imageUrl,
    name = name,
    tagline = tagline,
    firstBrewed = firstBrewed,
    volume = volume.value.toString() + String.space() + volume.unit,
    abv = abv,
    ibu = ibu,
    ebc = ebc,
    srm = srm,
    ph = ph,
    contributedBy = contributedBy,
    description = description,
    foodPairing = foodPairing.joinToString(),
    brewersTips = brewersTips,
    page = 0
)

fun BeerEntity.toBeersModelItem() = BeersModel(
    abv = abv,
    brewersTips = brewersTips,
    contributedBy = contributedBy,
    description = description,
    ebc = ebc,
    firstBrewed = firstBrewed,
    foodPairing = foodPairing,
    ibu = ibu,
    id = id,
    imageUrl = imageUrl,
    name = name,
    ph = ph,
    srm = srm,
    tagline = tagline,
    volume = volume
)

fun BeersModel.toRecentlyEntity() = RecentlyEntity(
    id = id,
    imageUrl = imageUrl,
    name = name,
    tagline = tagline,
    firstBrewed = firstBrewed,
    volume = volume,
    abv = abv,
    ibu = ibu,
    ebc = ebc,
    srm = srm,
    ph = ph,
    contributedBy = contributedBy,
    description = description,
    foodPairing = foodPairing,
    brewersTips = brewersTips
)

fun RecentlyEntity.toRecentlyModel() = RecentlyBeersModel(
    id = id,
    imageUrl = imageUrl,
    name = name,
    tagline = tagline,
    firstBrewed = firstBrewed,
    volume = volume,
    abv = abv,
    ibu = ibu,
    ebc = ebc,
    srm = srm,
    ph = ph,
    contributedBy = contributedBy,
    description = description,
    foodPairing = foodPairing,
    brewersTips = brewersTips
)
