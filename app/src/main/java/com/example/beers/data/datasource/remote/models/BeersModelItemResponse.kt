package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class BeersModelItemResponse(
    @SerializedName("abv") val abv: Double,
    @SerializedName("attenuationLevel") val attenuationLevel: Double,
    @SerializedName("boil_volume") val boilVolume: BoilVolumeResponse,
    @SerializedName("brewers_tips") val brewersTips: String,
    @SerializedName("contributed_by") val contributedBy: String,
    @SerializedName("description") val description: String,
    @SerializedName("ebc") val ebc: Double,
    @SerializedName("first_brewed") val firstBrewed: String,
    @SerializedName("food_pairing") val foodPairing: List<String>,
    @SerializedName("ibu") val ibu: Double,
    @SerializedName("id") val id: Int,
    @SerializedName("image_url") val imageUrl: String?,
    @SerializedName("ingredients") val ingredients: IngredientsResponse,
    @SerializedName("method") val method: MethodResponse,
    @SerializedName("name")  val name: String,
    @SerializedName("ph")  val ph: Double,
    @SerializedName("srm") val srm: Double,
    @SerializedName("tagline")  val tagline: String,
    @SerializedName("target_fg") val targetFg: Int,
    @SerializedName("target_Og") val targetOg: Double,
    @SerializedName("volume") val volume: VolumeResponse
)
