package com.example.beers.data.datasource.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.beers.data.datasource.local.dao.BeersDao
import com.example.beers.data.datasource.local.dao.RecentlyDao
import com.example.beers.data.datasource.local.dao.RemoteKeysDao
import com.example.beers.data.datasource.local.entities.BeerEntity
import com.example.beers.data.datasource.local.entities.RecentlyEntity
import com.example.beers.data.datasource.local.entities.RemoteKeys

@Database(entities = [RecentlyEntity::class, BeerEntity::class, RemoteKeys::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract fun recentlyDao(): RecentlyDao

    abstract fun beersDao(): BeersDao

    abstract fun getRemoteKeysDao(): RemoteKeysDao

    companion object {

        fun build(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, "test_database.db")
            .build()
    }
}
