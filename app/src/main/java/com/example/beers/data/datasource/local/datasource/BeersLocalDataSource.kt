package com.example.beers.data.datasource.local.datasource

import com.example.beers.data.datasource.local.dao.BeersDao
import com.example.beers.data.datasource.local.dao.RecentlyDao
import com.example.beers.data.datasource.local.dao.RemoteKeysDao
import com.example.beers.data.datasource.local.entities.BeerEntity
import com.example.beers.data.datasource.local.entities.RecentlyEntity
import com.example.beers.data.datasource.local.entities.RemoteKeys
import javax.inject.Inject

class BeersLocalDataSource @Inject constructor(
    private val recentlyDao: RecentlyDao,
    private val beersDao: BeersDao,
    private val remoteKeysDao: RemoteKeysDao,
) {

    suspend fun storeRecently(recentlyEntity: RecentlyEntity) = recentlyDao.insert(recentlyEntity)

    suspend fun getAllRecently() = recentlyDao.getAll()

    suspend fun deleteAll() = recentlyDao.deleteAll()

    suspend fun insertAllRemoteKeys(remoteKeys: List<RemoteKeys>) = remoteKeysDao.insertAll(remoteKeys)

    suspend fun getCreationTime() = remoteKeysDao.getCreationTime()

    suspend fun getRemoteKeyByBeerID(id: Int) = remoteKeysDao.getRemoteKeyByBeerID(id)


    suspend fun clearRemoteKeys() = remoteKeysDao.clearRemoteKeys()

    suspend fun insertAll(beers: List<BeerEntity>) = beersDao.insertAll(beers)

    fun getBeers() = beersDao.getBeers()

    suspend fun clearAllBeers() = beersDao.clearAllBeers()
}
