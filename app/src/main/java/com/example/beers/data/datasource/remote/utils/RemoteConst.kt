package com.example.beers.data.datasource.remote.utils

/**
 * The [Path] object is used to stored the
 * paths used in web services of application.
 */
object Path {
    const val BEERS = "beers"
    const val BEERS_BY_ID = "beers/{id}"
}

/**
 * The [Api] object is used to stored the
 * general values used in web services of application.
 */
object Api {
    const val v2 = "v2/"
}

/**
 * The [Values] object is used to stored the
 * specific values used in web services of application.
 */
object Values {
    const val TOTAL_PAGE = 20
}

/**
 * The [QueryParams] object is used to stored the
 * params used in web services of application.
 */
object QueryParams {
    const val PER_PAGE = "per_page"
    const val PAGE = "page"
}

/**
 * The [Params] object is used to stored the
 * params used in web services of application.
 */
object Params {
    const val BEER_ID = "id"
}
