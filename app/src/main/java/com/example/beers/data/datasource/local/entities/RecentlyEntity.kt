package com.example.beers.data.datasource.local.entities

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity
@Parcelize
data class RecentlyEntity(
    @PrimaryKey
    val id: Int,
    val imageUrl: String?,
    val name: String,
    val tagline: String,
    val firstBrewed: String,
    val volume: String,
    val abv: Double,
    val ibu: Double,
    val ebc: Double,
    val srm: Double,
    val ph: Double,
    val contributedBy: String,
    val description: String,
    val foodPairing: String,
    val brewersTips: String
) : Parcelable
