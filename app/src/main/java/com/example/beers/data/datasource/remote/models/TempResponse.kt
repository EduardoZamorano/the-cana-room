package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class TempResponse(
    @SerializedName("unit") val unit: String,
    @SerializedName("value") val value: Int
)
