package com.example.beers.data.datasource.remote

import com.example.beers.data.datasource.remote.api.BeersApi
import javax.inject.Inject

class BeersRemoteDataSource @Inject constructor(private val beersApi: BeersApi) {

    suspend fun fetchBeers(page: Int?) = beersApi.fetchBeers(page)

    suspend fun fetchBeersById(id: String) = beersApi.fetchBeersById(id)
}
