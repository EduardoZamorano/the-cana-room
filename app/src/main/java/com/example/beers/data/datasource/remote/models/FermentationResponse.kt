package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class FermentationResponse(
    @SerializedName("temp") val temp: TempResponse
)
