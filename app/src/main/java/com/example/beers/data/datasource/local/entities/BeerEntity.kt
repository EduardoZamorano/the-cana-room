package com.example.beers.data.datasource.local.entities

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.parcelize.Parcelize

@Entity(tableName = "beers")
@Parcelize
data class BeerEntity(
    @PrimaryKey(autoGenerate = false)
    val id: Int,
    @ColumnInfo(name = "image_url")
    val imageUrl: String?,
    val name: String,
    val tagline: String,
    @ColumnInfo(name = "first_brewed")
    val firstBrewed: String,
    val volume: String,
    val abv: Double,
    val ibu: Double,
    val ebc: Double,
    val srm: Double,
    val ph: Double,
    @ColumnInfo(name = "contributed_by")
    val contributedBy: String,
    val description: String,
    @ColumnInfo(name = "food_pairing")
    val foodPairing: String,
    @ColumnInfo(name = "brewers_tips")
    val brewersTips: String,
    @ColumnInfo(name = "page")
    var page: Int,
) : Parcelable
