package com.example.beers.data.datasource.remote.api

import com.example.beers.data.datasource.remote.models.BeersResponse
import com.example.beers.data.datasource.remote.utils.Api.v2
import com.example.beers.data.datasource.remote.utils.Params.BEER_ID
import com.example.beers.data.datasource.remote.utils.Path.BEERS
import com.example.beers.data.datasource.remote.utils.Path.BEERS_BY_ID
import com.example.beers.data.datasource.remote.utils.QueryParams.PAGE
import com.example.beers.data.datasource.remote.utils.QueryParams.PER_PAGE
import com.example.beers.data.datasource.remote.utils.Values.TOTAL_PAGE
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BeersApi {

    @GET("$v2$BEERS")
    suspend fun fetchBeers(
        @Query(PAGE) page: Int?,
        @Query(PER_PAGE) items: Int = TOTAL_PAGE
    ): BeersResponse

    @GET("$v2$BEERS_BY_ID")
    suspend fun fetchBeersById(@Path(BEER_ID) id: String): BeersResponse
}
