package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class MethodResponse(
    @SerializedName("fermentation") val fermentation: FermentationResponse,
    @SerializedName("mash_temp") val mashTemp: List<MashTempResponse>,
    @SerializedName("twist") val twist: String?
)
