package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class MashTempResponse(
    @SerializedName("duration") val duration: Int,
    @SerializedName("temp") val temp: TempResponse
)
