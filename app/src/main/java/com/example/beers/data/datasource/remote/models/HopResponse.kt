package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class HopResponse(
    @SerializedName("add") val add: String,
    @SerializedName("amount") val amount: AmountResponse,
    @SerializedName("attribute") val attribute: String,
    @SerializedName("name") val name: String
)
