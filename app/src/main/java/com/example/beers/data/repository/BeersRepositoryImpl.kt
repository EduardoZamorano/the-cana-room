package com.example.beers.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import androidx.paging.map
import com.example.beers.utils.ResponseState
import com.example.beers.data.datasource.local.datasource.BeersLocalDataSource
import com.example.beers.data.datasource.remote.BeersRemoteDataSource
import com.example.beers.data.datasource.remote.utils.Values.TOTAL_PAGE
import com.example.beers.data.toBeers
import com.example.beers.data.toBeersModelItem
import com.example.beers.data.toRecentlyEntity
import com.example.beers.data.toRecentlyModel
import com.example.beers.di.viewmodel.DefaultDispatcher
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.models.RecentlyBeersModel
import com.example.beers.domain.repository.BeersRepository
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class BeersRepositoryImpl @Inject constructor(
    private val beersRemoteDataSource: BeersRemoteDataSource,
    private val beersLocalDataSource: BeersLocalDataSource,
    @DefaultDispatcher private val defaultDispatcher: CoroutineDispatcher
) : BeersRepository {

    @OptIn(ExperimentalPagingApi::class)
    override fun fetchBeers(): Flow<PagingData<BeersModel>> = Pager(
        config = PagingConfig(
            pageSize = TOTAL_PAGE
        ),
        pagingSourceFactory = {
            beersLocalDataSource.getBeers()
        },
        remoteMediator = BeersRemoteMediator(
            beersRemoteDataSource,
            beersLocalDataSource,
        ),
    ).flow.map { pagingData ->
        pagingData.map { it.toBeersModelItem() }
    }

    override suspend fun fetchBeersById(id: String): Flow<ResponseState<List<BeersModel>>> = flow {
        try {
            val responseApi = beersRemoteDataSource.fetchBeersById(id).toBeers()
            if (responseApi != null)
                beersLocalDataSource.storeRecently(responseApi[0].toRecentlyEntity())

            emit(ResponseState.Success(responseApi))
        } catch (ex: Exception) {
            Timber.e(ex); emit(ResponseState.Failure(ex))
        }
    }.flowOn(defaultDispatcher)

    override suspend fun getAllRecently(): Flow<List<RecentlyBeersModel>> = flow {
        emit(beersLocalDataSource.getAllRecently().map { it.toRecentlyModel() })
    }.flowOn(defaultDispatcher)

    override suspend fun deleteAll() = beersLocalDataSource.deleteAll()
}
