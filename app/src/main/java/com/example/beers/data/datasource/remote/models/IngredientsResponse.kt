package com.example.beers.data.datasource.remote.models

import com.google.gson.annotations.SerializedName

data class IngredientsResponse(
    @SerializedName("hops") val hops: List<HopResponse>,
    @SerializedName("malt") val malt: List<MaltResponse>,
    @SerializedName("yeast") val yeast: String?
)
