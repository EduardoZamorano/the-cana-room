package com.example.beers.data.datasource.local.dao

import androidx.paging.PagingSource
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.beers.data.datasource.local.entities.BeerEntity

@Dao
interface BeersDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(beers: List<BeerEntity>)

    @Query("Select * From beers Order By page")
    fun getBeers(): PagingSource<Int, BeerEntity>

    @Query("Delete From beers")
    suspend fun clearAllBeers()
}