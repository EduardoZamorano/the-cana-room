package com.example.beers.data.datasource.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.beers.data.datasource.local.entities.RecentlyEntity

@Dao
interface RecentlyDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(recently: RecentlyEntity) : Long

    @Query("SELECT * FROM RecentlyEntity")
    suspend fun getAll(): List<RecentlyEntity>

    @Query("DELETE FROM RecentlyEntity")
    suspend fun deleteAll()
}
