package com.example.beers.presentation.ui.detail

import android.annotation.SuppressLint
import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.beers.R
import com.example.beers.presentation.ui.base.composables.LoadingCircular
import com.example.beers.presentation.ui.base.composables.NotResults
import com.example.beers.presentation.ui.detail.screen.DetailScreen
import com.example.beers.presentation.viewmodels.detail.DetailViewModel
import com.example.beers.utils.ResponseState

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun DetailFragment(
    modifier: Modifier = Modifier,
    id: Int = -1,
    detailViewModel: DetailViewModel = hiltViewModel(),
    onClickBackScreen: () -> Unit = {}
) {

    LaunchedEffect(key1 = Unit) {
        detailViewModel.getBeersById(id.toString())
    }

    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.primary,
                title = {
                    Text(
                        text = stringResource(id = R.string.detail_title),
                        color = Color.White
                    )
                },
                navigationIcon = {
                    IconButton(onClick = { onClickBackScreen.invoke() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                }
            )
        },
        content = {
            when (val beerItems = detailViewModel.beerState.value) {
                is ResponseState.Loading -> {
                    LoadingCircular(modifier = modifier
                        .fillMaxWidth()
                        .fillMaxHeight())
                }
                is ResponseState.Success -> {
                    DetailScreen(beerItem = beerItems.data?.first(), modifier = modifier)
                }
                is ResponseState.Failure -> {
                    NotResults()
                }
            }
        }
    )
}
