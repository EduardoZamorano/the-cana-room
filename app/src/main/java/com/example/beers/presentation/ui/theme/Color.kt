package com.example.beers.presentation.ui.theme

import androidx.compose.ui.graphics.Color

val Blue900 = Color(0xFF1565C0)
val Teal200 = Color(0xFF03DAC5)
val ColorPrimary = Color(0xFF4267B2)