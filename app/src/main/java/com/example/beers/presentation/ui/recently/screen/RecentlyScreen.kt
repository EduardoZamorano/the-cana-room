package com.example.beers.presentation.ui.recently.screen

import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.itemsIndexed
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import com.example.beers.domain.models.RecentlyBeersModel
import com.example.beers.presentation.ui.base.composables.NotResults
import com.example.beers.presentation.ui.base.composables.BeerItem
import com.example.beers.presentation.ui.theme.BeersApplicationComposeTheme
import com.example.beers.utils.Const

@Composable
fun RecentlyScreen(
    modifier: Modifier = Modifier,
    list: List<RecentlyBeersModel> = emptyList(),
    onClickToDetailScreen: (Int) -> Unit = {},
) {

    if (list.isEmpty()) {
        NotResults()
        return
    }

    LazyVerticalGrid(
        modifier = modifier,
        columns = GridCells.Fixed(Const.GRID_CELL)
    ) {
        itemsIndexed(items = list) { _, item ->
            BeerItem(item.imageUrl, item.name, item.tagline, item.firstBrewed) {
                onClickToDetailScreen(item.id)
            }
        }
    }
}

@Preview(showBackground = true)
@Composable
fun HomeScreenPreview() {
    BeersApplicationComposeTheme {
        RecentlyScreen()
    }
}
