package com.example.beers.presentation.ui.detail.screen

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.wrapContentWidth
import androidx.compose.foundation.rememberScrollState
import androidx.compose.foundation.verticalScroll
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.colorResource
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Devices
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.tooling.preview.PreviewParameter
import androidx.compose.ui.unit.dp
import com.example.beers.R
import com.example.beers.domain.models.BeersModel
import com.example.beers.presentation.ui.base.composables.LoadImage
import com.example.beers.presentation.ui.theme.TextStyle_Info
import com.example.beers.presentation.ui.theme.TextStyle_MainTitle
import com.example.beers.presentation.ui.theme.TextStyle_SecondTitle
import com.example.beers.presentation.ui.theme.bold

@Preview(showBackground = true, device = Devices.PIXEL)
@Composable
fun DetailScreen(modifier: Modifier = Modifier,
    @PreviewParameter(BeersModelDataProvider::class) beerItem: BeersModel?) {

    if(beerItem == null) return

    Column(
        modifier = modifier
            .verticalScroll(rememberScrollState())
            .fillMaxWidth()
            .padding(horizontal = 16.dp, vertical = 20.dp)

    ) {
        BeerName(name = beerItem.name)
        BeerTagline(tagline = beerItem.tagline)
        BeerBrewed(date = beerItem.firstBrewed)
        BeerDetails(item = beerItem)
        BeerDescription(description = beerItem.description)
        BeerFood(food = beerItem.foodPairing)
        BeerBrewersTips(tips = beerItem.brewersTips)
    }

}

@Composable
fun BeerName(name: String) {
    Text(
        text = name,
        style = TextStyle_MainTitle,
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    )
}

@Composable
fun BeerTagline(tagline: String) {
    Text(
        text = tagline,
        style = TextStyle_SecondTitle,
        modifier = Modifier
            .padding(top = 5.dp)
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    )
}

@Composable
fun BeerBrewed(date: String) {
    Text(
        text = date,
        style = TextStyle_SecondTitle,
        color = colorResource(id = android.R.color.darker_gray),
        modifier = Modifier
            .padding(top = 5.dp)
            .fillMaxWidth()
            .wrapContentWidth(Alignment.CenterHorizontally)
    )
}

@Composable
fun BeerDetails(item: BeersModel) {
    Row(Modifier.padding(top = 20.dp)) {
        LoadImage(item.imageUrl, 200.dp)
        Spacer(modifier = Modifier.padding(5.dp))
        BeerMoreDetails(item)
    }
}

@Composable
fun BeerMoreDetails(item: BeersModel) {
    Column(
        modifier = Modifier.fillMaxWidth()
    ) {
        Row {
            MoreDetailItem(
                stringRes = R.string.detail_volume,
                detail = item.volume
            )
            MoreDetailItem(
                stringRes = R.string.detail_abv,
                detail = item.abv.toString(),
                maxWidth = 1f
            )
        }

        Row(modifier = Modifier.padding(top = 30.dp)) {
            MoreDetailItem(
                stringRes = R.string.detail_ibu,
                detail = item.ibu.toString()
            )
            MoreDetailItem(
                stringRes = R.string.detail_ebc,
                detail = item.ebc.toString(),
                maxWidth = 1f
            )
        }

        Row(modifier = Modifier.padding(top = 30.dp)) {
            MoreDetailItem(
                stringRes = R.string.detail_srm,
                detail = item.srm.toString()
            )
            MoreDetailItem(
                stringRes = R.string.detail_ph,
                detail = item.ph.toString(),
                maxWidth = 1f
            )
        }
    }
}

@Composable
fun MoreDetailItem(@StringRes stringRes: Int, detail: String, maxWidth: Float = 0.5f) {
    Column(
        modifier = Modifier
            .fillMaxWidth(maxWidth)
    ) {
        Text(
            text = stringResource(id = stringRes),
            style = TextStyle_Info,
        )
        Text(
            text = detail,
            style = TextStyle_Info.bold(),
            color = colorResource(id = android.R.color.darker_gray),
        )
    }
}

@Composable
fun BeerDescription(description: String) {
    Column {
        TextDescriptionTitle(R.drawable.ic_description, R.string.detail_description)
        TextDescription(text = description)
    }
}

@Composable
fun BeerFood(food: String) {
    Column {
        TextDescriptionTitle(R.drawable.ic_food_pairing, R.string.detail_food_pairing)
        TextDescription(text = food)
    }
}

@Composable
fun BeerBrewersTips(tips: String) {
    Column {
        TextDescriptionTitle(R.drawable.ic_brewers_tips, R.string.detail_brewers_tips)
        TextDescription(text = tips)
    }
}

@Composable
fun TextDescriptionTitle(@DrawableRes drawable: Int, @StringRes stringRes: Int) {
    Row(
        modifier = Modifier.padding(top = 20.dp)
    ) {
        Image(
            painter = painterResource(drawable),
            contentDescription = null
        )
        Spacer(modifier = Modifier.padding(horizontal = 8.dp))
        Text(
            text = stringResource(
                id = stringRes
            ),
            style = TextStyle_SecondTitle,
        )
    }
}

@Composable
fun TextDescription(text: String) {
    Text(
        text = text,
        color = colorResource(id = android.R.color.darker_gray),
        style = TextStyle_Info,
        modifier = Modifier
            .padding(top = 8.dp)
            .fillMaxWidth()
    )
}
