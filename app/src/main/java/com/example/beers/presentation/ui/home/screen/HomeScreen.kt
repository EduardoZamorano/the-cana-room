package com.example.beers.presentation.ui.home.screen

import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.GridItemSpan
import androidx.compose.foundation.lazy.grid.LazyGridItemSpanScope
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.paging.LoadState
import androidx.paging.compose.LazyPagingItems
import androidx.paging.compose.itemContentType
import androidx.paging.compose.itemKey
import com.example.beers.domain.models.BeersModel
import com.example.beers.presentation.ui.base.composables.BeerItem
import com.example.beers.presentation.ui.base.composables.EmptyItem
import com.example.beers.presentation.ui.base.composables.LoadingCircular
import com.example.beers.presentation.ui.base.composables.RetryButton
import com.example.beers.presentation.ui.base.composables.RetryItem
import com.example.beers.presentation.ui.base.composables.rememberLazyListState

@Composable
fun HomeScreen(
    modifier: Modifier = Modifier,
    onClickToDetailScreen: (Int) -> Unit = {},
    beers: LazyPagingItems<BeersModel>
) {

    LazyVerticalGrid(
        columns = GridCells.Fixed(2),
        state = beers.rememberLazyListState(),
        modifier = modifier.fillMaxSize(),
        verticalArrangement = if (beers.itemCount < 1) Arrangement.Center else Arrangement.Top
    ) {
        when (beers.loadState.refresh) {
            is LoadState.Loading -> item(span = span()) { LoadingCircular(size = 64.dp) }
            is LoadState.Error -> item(span = span()) { RetryButton { beers.refresh() } }
            is LoadState.NotLoading -> if (beers.itemCount < 1) { item(span = span()) { EmptyItem() } }
        }

        items(
            count = beers.itemCount,
            key = beers.itemKey { it.id },
            contentType = beers.itemContentType { it.name }
        ) { index ->
            val beer = beers[index]
            beer?.let { item ->
                BeerItem(item.imageUrl, item.name, item.tagline, item.firstBrewed) {
                    onClickToDetailScreen(item.id)
                }
            }
        }

        when (beers.loadState.append) {
            is LoadState.Loading -> item { LoadingCircular() }
            is LoadState.Error -> item { RetryItem { beers.retry() } }
            is LoadState.NotLoading -> Unit
        }
    }
}

private fun span(): (LazyGridItemSpanScope.() -> GridItemSpan) = { GridItemSpan(2) }
