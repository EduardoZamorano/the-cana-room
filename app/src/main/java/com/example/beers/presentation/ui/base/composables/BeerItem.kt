package com.example.beers.presentation.ui.base.composables

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.heightIn
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.shape.CornerSize
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Card
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.beers.presentation.ui.theme.TextStyle_Info
import com.example.beers.presentation.ui.theme.bold

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun BeerItem(
    url: String?,
    name: String,
    tagline: String,
    firstBrewed: String,
    listener: (() -> Unit)? = null
) {
    Card(
        modifier = Modifier
            .padding(horizontal = 8.dp, vertical = 8.dp)
            .heightIn(min = 200.dp)
            .fillMaxWidth(),
        elevation = 4.dp,
        backgroundColor = MaterialTheme.colors.background,
        shape = RoundedCornerShape(corner = CornerSize(10.dp)),
        onClick = { listener?.invoke() }
    ) {
        Column(
            modifier = Modifier
                .padding(8.dp)
                .height(intrinsicSize = IntrinsicSize.Max)
                .fillMaxWidth(),
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            LoadImage(url)
            Text(
                modifier = Modifier.padding(top = 8.dp),
                text = name,
                style = TextStyle_Info.bold(),
                textAlign = TextAlign.Center
            )
            Text(
                modifier = Modifier.padding(top = 5.dp),
                text = tagline,
                style = TextStyle_Info,
                maxLines = 2,
                overflow = TextOverflow.Ellipsis
            )
            Text(
                modifier = Modifier.padding(top = 5.dp),
                text = firstBrewed,
                style = TextStyle_Info
            )
        }
    }
}

@Preview
@Composable
fun RecentlyBeersListItemPreview() {
    BeerItem(
        url = "https://images.punkapi.com/v2/25.png",
        name = "Nelson Sauvin",
        tagline = "A Real Bitter Experience.",
        firstBrewed = "02/2012"
    )
}
