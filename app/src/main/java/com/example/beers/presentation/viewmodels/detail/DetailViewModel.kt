package com.example.beers.presentation.viewmodels.detail

import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.beers.utils.ResponseState
import com.example.beers.domain.models.BeersModel
import com.example.beers.domain.usecases.GetBeersByIdUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val getBeersByIdUseCase: GetBeersByIdUseCase) : ViewModel() {

    private val _beerState = mutableStateOf<ResponseState<List<BeersModel>>>(ResponseState.Success(null))
    val beerState: State<ResponseState<List<BeersModel>>> = _beerState

    fun getBeersById(id: String) {
        viewModelScope.launch {
            getBeersByIdUseCase(id).collect { response ->
                _beerState.value = response
            }
        }
    }
}
