package com.example.beers.presentation.ui.detail.screen

import androidx.compose.ui.tooling.preview.PreviewParameterProvider
import com.example.beers.domain.models.BeersModel

class BeersModelDataProvider : PreviewParameterProvider<BeersModel> {
    override val values: Sequence<BeersModel>
        get() = sequenceOf(
            BeersModel(
                abv = 2.0,
                brewersTips = "Ferment at higher or lower temperatures to select the esters that will create",
                contributedBy = "Sam Mason <samjbmason>",
                description = "2008 Prototype beer, a 4.7% wheat ale wit",
                ebc = 8.0,
                firstBrewed = "02/2012",
                foodPairing = "Haggis bon bons",
                ibu = 90.0,
                id = 25,
                imageUrl = "https://images.punkapi.com/v2/25.png",
                name = "Nelson Sauvin",
                ph = 4.4,
                srm = 6.0,
                tagline = "A Real Bitter Experience.",
                volume = "20 litres"
            )
        )
}
