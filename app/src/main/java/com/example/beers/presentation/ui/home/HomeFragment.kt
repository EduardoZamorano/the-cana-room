package com.example.beers.presentation.ui.home

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.paging.compose.LazyPagingItems
import com.example.beers.domain.models.BeersModel
import com.example.beers.extensions.empty
import com.example.beers.presentation.ui.home.composables.MainAppBar
import com.example.beers.presentation.ui.home.screen.HomeScreen
import com.example.beers.presentation.viewmodels.home.HomeViewModel
import com.example.beers.utils.SearchWidgetState

@Composable
fun HomeFragment(
    beers: LazyPagingItems<BeersModel>,
    homeViewModel: HomeViewModel = hiltViewModel(),
    onClickToDetailScreen: (Int) -> Unit = {},
    onClickAction: () -> Unit = {},
) {

    val searchWidgetState by homeViewModel.searchWidgetState
    val searchTextState by homeViewModel.searchTextState

    Scaffold(
        topBar = {
            MainAppBar(
                searchWidgetState = searchWidgetState,
                searchTextState = searchTextState,
                onTextChange = {
                    homeViewModel.updateSearchTextState(it)
                },
                onCloseClicked = {
                    homeViewModel.updateSearchTextState(String.empty())
                    homeViewModel.updateSearchWidgetState(SearchWidgetState.CLOSED)
                },
                onSearchClicked = {
                    onClickToDetailScreen(it.toIntOrNull() ?: 0)
                },
                onSearchTriggered = {
                    homeViewModel.updateSearchWidgetState(SearchWidgetState.OPENED)
                },
                onClickAction = {
                    onClickAction()
                },
            )
        }
    ) { paddingValues ->

        Box(
            modifier = Modifier
                .fillMaxSize()
                .padding(paddingValues),
        ) {
            HomeScreen(
                beers = beers,
                onClickToDetailScreen = onClickToDetailScreen
            )
        }
    }
}
