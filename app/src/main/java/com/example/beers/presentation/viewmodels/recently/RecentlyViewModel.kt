package com.example.beers.presentation.viewmodels.recently

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.beers.domain.models.RecentlyBeersModel
import com.example.beers.domain.usecases.DeleteRecentlyUseCase
import com.example.beers.domain.usecases.GetAllRecentlyUseCase
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RecentlyViewModel @Inject constructor(
    private val getAllRecentlyUseCase: GetAllRecentlyUseCase,
    private val deleteRecentlyUseCase: DeleteRecentlyUseCase,
) : ViewModel() {

    private var _list = mutableStateOf(listOf<RecentlyBeersModel>())
    val list: MutableState<List<RecentlyBeersModel>> = _list

    fun getRecently() = viewModelScope.launch {
        getAllRecentlyUseCase()
            .collect {
                list.value = it
            }
    }

    fun deleteAll() = viewModelScope.launch {
        deleteRecentlyUseCase()
        getAllRecentlyUseCase().collect {
            list.value = it
        }
    }
}
