package com.example.beers.presentation.ui.main.activities

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import androidx.paging.compose.collectAsLazyPagingItems
import com.example.beers.presentation.ui.detail.DetailFragment
import com.example.beers.presentation.ui.home.HomeFragment
import com.example.beers.presentation.ui.recently.RecentlyFragment
import com.example.beers.presentation.ui.splash.SplashFragment
import com.example.beers.presentation.ui.theme.BeersApplicationComposeTheme
import com.example.beers.presentation.viewmodels.home.HomeViewModel
import com.example.beers.utils.Const.DETAIL_BEER_ID
import com.example.beers.utils.Route
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            BeersApplicationComposeTheme {
                AppScreen()
            }
        }
    }
}

@Composable
fun AppScreen() {
    val navController = rememberNavController()
    val homeViewModel: HomeViewModel = hiltViewModel()
    val beers = remember { homeViewModel.getBeers() }.collectAsLazyPagingItems()

    NavHost(
        navController = navController,
        startDestination = Route.Splash.route,
    ) {

        composable(route = Route.Splash.route) {
            SplashFragment(
                onAnimationEnd = {
                    navController.navigate(Route.Home.route) {
                        popUpTo(Route.Splash.route) {
                            inclusive = true
                        }
                    }
                }
            )
        }

        composable(route = Route.Home.route) {
            HomeFragment(
                beers = beers,
                homeViewModel = homeViewModel,
                onClickToDetailScreen = {
                    navController.navigate(Route.Detail.createRoute(it))
                }
            ) {
                navController.navigate(Route.Recently.route)
            }
        }

        composable(route = Route.Recently.route) {
            RecentlyFragment(
                onClickToDetailScreen = {
                    navController.navigate(Route.Detail.createRoute(it))
                }, onClickBackScreen = {
                    navController.popBackStack()
                }
            )
        }

        composable(
            route = Route.Detail.route,
            arguments = listOf(
                navArgument(DETAIL_BEER_ID) {
                    type = NavType.IntType
                }
            )
        ) { backStackEntry ->
            val beerId = backStackEntry.arguments?.getInt(DETAIL_BEER_ID)
            requireNotNull(beerId) { "beerId parameter wasn't found. Please make sure it's set!" }
            DetailFragment(id = beerId) {
                navController.popBackStack()
            }
        }
    }
}
