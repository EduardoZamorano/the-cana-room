package com.example.beers.presentation.ui.base.composables

import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.material.Button
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.beers.R

@Composable
fun RetryButton(
    modifier: Modifier = Modifier,
    onTryAgainClick: () -> Unit
) {
    Box(
        modifier = modifier.fillMaxWidth(),
        contentAlignment = Alignment.Center
    ) {
        Button(
            modifier = modifier,
            onClick = onTryAgainClick
        ) {
            Text(
                text = stringResource(id = R.string.try_again),
                color = Color.White
            )
        }
    }
}
