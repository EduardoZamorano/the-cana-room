package com.example.beers.presentation.ui.recently

import android.annotation.SuppressLint
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Delete
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.hilt.navigation.compose.hiltViewModel
import com.example.beers.R
import com.example.beers.presentation.ui.recently.screen.RecentlyScreen
import com.example.beers.presentation.viewmodels.recently.RecentlyViewModel

@SuppressLint("UnusedMaterialScaffoldPaddingParameter")
@Composable
fun RecentlyFragment(
    modifier: Modifier = Modifier,
    recentlyViewModel: RecentlyViewModel = hiltViewModel(),
    onClickToDetailScreen: (Int) -> Unit = {},
    onClickBackScreen: () -> Unit = {}
) {

    LaunchedEffect(key1 = Unit) {
        recentlyViewModel.getRecently()
    }

    Scaffold(
        topBar = {
            TopAppBar(
                backgroundColor = MaterialTheme.colors.primary,
                title = {
                    Text(
                        text = stringResource(id = R.string.recently_seen),
                        color = Color.White
                    )
                },
                navigationIcon = {
                    IconButton(onClick = { onClickBackScreen.invoke() }) {
                        Icon(
                            imageVector = Icons.Default.ArrowBack,
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                },
                actions = {
                    IconButton(onClick = { recentlyViewModel.deleteAll() }) {
                        Icon(
                            imageVector = Icons.Default.Delete,
                            contentDescription = null,
                            tint = Color.White
                        )
                    }
                }
            )
        },
        content = {
            RecentlyScreen(
                modifier = modifier,
                list = recentlyViewModel.list.value,
                onClickToDetailScreen = onClickToDetailScreen,
            )
        }
    )
}
