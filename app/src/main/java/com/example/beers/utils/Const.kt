package com.example.beers.utils

object Const {
    const val SPLASH_SCREEN = "splash"
    const val HOME_SCREEN = "home"
    const val DETAIL_SCREEN = "detail"
    const val RECENTLY_SCREEN = "recently"

    const val DETAIL_BEER_ID = "beerId"
    const val GRID_CELL = 2
}