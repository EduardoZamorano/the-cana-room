package com.example.beers.utils

enum class SearchWidgetState {
    OPENED,
    CLOSED
}
