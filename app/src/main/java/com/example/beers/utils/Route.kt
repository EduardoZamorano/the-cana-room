package com.example.beers.utils

import com.example.beers.utils.Const.DETAIL_BEER_ID
import com.example.beers.utils.Const.DETAIL_SCREEN
import com.example.beers.utils.Const.HOME_SCREEN
import com.example.beers.utils.Const.RECENTLY_SCREEN
import com.example.beers.utils.Const.SPLASH_SCREEN

sealed class Route(val route: String) {
    object Splash: Route(SPLASH_SCREEN)
    object Home: Route(HOME_SCREEN)
    object Recently: Route(RECENTLY_SCREEN)
    object Detail: Route("$DETAIL_SCREEN/{$DETAIL_BEER_ID}") {
        fun createRoute(beerId: Int) = "$DETAIL_SCREEN/$beerId"
    }
}