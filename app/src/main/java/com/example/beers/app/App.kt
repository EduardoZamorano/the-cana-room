package com.example.beers.app

import android.app.Application
import com.example.beers.BuildConfig
import dagger.hilt.android.HiltAndroidApp
import timber.log.Timber

@HiltAndroidApp
class App : Application() {

    override fun onCreate() {
        super.onCreate()
        initTimber()
    }

    /**
     * Plant a [Timber] tree to log only in
     * debug mode.
     */
    private fun initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }
}
