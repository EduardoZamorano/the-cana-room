plugins {
    id("com.android.application")
    id("dagger.hilt.android.plugin")
    id("com.google.gms.google-services")
    id("com.google.firebase.crashlytics")
    id("org.jetbrains.kotlin.plugin.compose") version "2.0.0"
    kotlin("android")
    kotlin("plugin.parcelize")
    kotlin("kapt")
}

android {
    compileSdk = Project.compileSdk
    namespace = "com.example.beers"

    defaultConfig {
        applicationId = "com.example.beers"
        minSdk = Project.minSdk
        targetSdk = Project.targetSdk
        versionCode = Project.versionCode
        versionName = Project.versionName

        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }

    buildTypes {
        named("release") {
            isMinifyEnabled = false
            isDebuggable = false
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            buildConfigField("String", "BASE_URL", "\"https://api.punkapi.com/\"")
        }
        named("debug") {
            isMinifyEnabled = false
            isDebuggable = true
            proguardFiles(getDefaultProguardFile("proguard-android.txt"), "proguard-rules.pro")
            buildConfigField("String", "BASE_URL", "\"https://api.punkapi.com/\"")
        }
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_17
        targetCompatibility = JavaVersion.VERSION_17
    }

    kotlinOptions {
        jvmTarget = "17"
    }

    buildFeatures {
        viewBinding = true
        compose = true
    }
}

dependencies {

    implementation(Dependencies.androidCore)
    implementation(Dependencies.appCompat)
    implementation(Dependencies.material)
    implementation(Dependencies.constraint)
    implementation(Dependencies.accompanistSystemUi)

    // Testing
    testImplementation(Dependencies.junit)
    testImplementation(Dependencies.mockitoInline)
    testImplementation(Dependencies.coroutinesTest)
    androidTestImplementation(Dependencies.espressoCore)
    androidTestImplementation(Dependencies.mockitoAndroid)
    api(Dependencies.mockitoCore)
    api(Dependencies.archCore)
    api(Dependencies.okhttpMockWebServer)

    // Timber Logs
    implementation(Dependencies.timber)

    // Retrofit
    implementation(Dependencies.retrofit)
    implementation(Dependencies.retrofitConverterGson)
    implementation(Dependencies.okhttpInterceptor)

    // Lifecycle
    implementation(Dependencies.lifecycleExtensions)
    implementation(Dependencies.lifecycleViewModel)
    implementation(Dependencies.lifecycleLiveData)

    // Dagger Hilt
    implementation(Dependencies.daggerHilt)
    kapt(Dependencies.daggerHiltCompiler)
    implementation(Dependencies.daggerHiltNavCompose)

    // Lottie
    implementation(Dependencies.lottieCompose)

    // Room
    implementation(Dependencies.roomRuntime)
    implementation(Dependencies.room)
    implementation(Dependencies.roomPaging)
    kapt(Dependencies.roomCompiler)

    // Compose
    implementation(Dependencies.activityCompose)
    implementation(Dependencies.coilCompose)
    implementation(Dependencies.lifecycleCompose)
    implementation(Dependencies.liveDataCompose)
    implementation(Dependencies.materialCompose)
    implementation(Dependencies.navigationCompose)
    implementation(Dependencies.pagingCompose)
    debugImplementation(Dependencies.toolingCompose)

    // Firebase
    implementation(platform(Dependencies.firebaseBoM))
    implementation(Dependencies.firebaseAnalytics)
    implementation(Dependencies.firebaseCrashlytics)
}