## Why use Coroutines? ##

* Help to manage long-running tasks that might otherwise block the main thread and cause your app to become unresponsive.

## Why use LiveData? ##

* Avoid memory leaks
* Ensures your UI matches your data state
* Only when the activity is in an active lifecycle state does the LiveData send an “on changed event"

## Why use Dagger? ##

* Improves your app's performance by releasing objects in memory when they're no longer needed.
* Makes it easier to manage the dependencies between the classes in our app
* Simplifies access to shared instances.
* Easier unit and integration testing

## Why use Retrofit? ##

* Help to manages the process of receiving, sending, and creating HTTP requests and responses.
* It is easy to use and understand
* There are so many formats and libraries to support converters
* Easily manage connect timeout and read timeout just using it’s methods and also adds Interceptor.

## Why use Mockito? ##

* Help to create a dummy functionality can be added to a mock interface that can be used in unit testing.

## Why use MockWebServer? ##

* Help to testing your API calls.
* You don’t need to make changes on the app side when using a MockWebServer
* Easy to use and create mock to test api calls.

## Why use View binding? ##

* Allows you to more easily write code that interacts with views
* Has an option to ignore a layout file while generating a binding class
* Null Safety
* The fields in each binding class have types matching the views they reference in the XML file

## Why use Glide? ##

* It provides animated GIF support and handles image loading/caching.
* Easy to fetch, resize, and display a remote images.
* Supports old android versions.

## Why use Room? ##

* Is now considered as a better approach for data persistence than SQLiteDatabase.
* Easily integrated with other Architecture components (LiveData)
* Convenience annotations that minimize repetitive and error-prone boilerplate code.
* Compile-time verification of SQL queries

## Why use Timber? ##

* It is not required to pass in the tag parameter in the Timber.d(...) or Timber.e(...), automatically detects the class in which logs were written.

## Why use Lottie? ##

* Allows apps to natively use Adobe After Effects animations in real time
* Supports .json files, you don't need to download heavy animation files
* Can play your animation on web or mobile devices with a nice high quality.
