# Design pattern - Model View ViewModel (MVVM)

![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/mvvm.png)


# Clean Architecture

![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/Clean_Architecture.png)
