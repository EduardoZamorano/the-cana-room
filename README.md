# README #

An incredible application for consulting different beers!

## About application ##

It is a sample of different types of beer, you will be able to see each one of them in detail, it also has a search engine and a view of recent views.

## Contents
1. [Architecture and pattern design](docs/architectural-pattern.md)

## Included Libraries

* [Kotlin][1]
* [Coroutines][2]
* [LiveData] [3]
* [Dagger 2][4]
* [Retrofit][5]
* [OkHttp][6]
* [Junit][7]
* [Mockito][8]
* [Espresso][9]
* [MockWebServer][10]
* [View-binding][11]
* [Glide][12]
* [Room][13]
* [Timber][14]
* [Lottie][15]

[See more from libraries](docs/libraries-explain.md)

## Screenshots ##

![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/1-splash.png)
![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/2-Listing.png)
![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/3-detail.png)
![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/4-recently.png)
![img](https://bitbucket.org/EduardoZamorano/the-cana-room/downloads/5-empty-results.png)

## Created by ##

* Eduardo Zamorano

[1]: https://kotlinlang.org/
[2]: https://developer.android.com/kotlin/coroutines
[3]: https://developer.android.com/topic/libraries/architecture/livedata
[4]: https://github.com/google/dagger
[5]: https://github.com/square/retrofit
[6]: https://github.com/square/okhttp
[7]: http://developer.android.com/intl/es/reference/junit/framework/package-summary.html
[8]: http://mockito.org/
[9]: https://developer.android.com/training/testing/espresso/index.html
[10]: https://github.com/square/okhttp/tree/master/mockwebserver
[11]: https://developer.android.com/topic/libraries/view-binding
[12]: https://github.com/bumptech/glide
[13]: https://developer.android.com/training/data-storage/room
[14]: https://github.com/ajalt/timberkt
[15]: https://github.com/airbnb/lottie-android
