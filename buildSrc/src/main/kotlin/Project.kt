object Project {
    const val compileSdk = 34
    const val minSdk = 23
    const val targetSdk = 34
    const val versionCode = 2
    const val versionName = "1.0.0"
}
